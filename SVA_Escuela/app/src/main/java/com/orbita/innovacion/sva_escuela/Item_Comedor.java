package com.orbita.innovacion.sva_escuela;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

public class Item_Comedor {

    private String ID;
    private String nombre;
    private String descripcion;
    private String img;

    public Item_Comedor(String Id, String nombre, String descripcion, String img) {
        this.ID = Id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.img = img;
    }

    public String getID() {
        return ID;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Bitmap getImg() {
        return base64ToBitmap(img);
    }

    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }
}
