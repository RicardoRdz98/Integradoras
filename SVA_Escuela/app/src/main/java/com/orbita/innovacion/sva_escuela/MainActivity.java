package com.orbita.innovacion.sva_escuela;

import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private TextView name, email, bienvenida;
    private CircleImageView foto;
    private String TEMA;
    private String pin = "", cargo = "", ID = "";

    private LottieAnimationView animationView;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    private CoordinatorLayout coordinatorLayout;

    private Menu miMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null){
                    setUserData(user);
                }else{
                    goLogInScreen();
                }
            }
        };

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.fram);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        foto = (CircleImageView) headerView.findViewById(R.id.photo);
        name  = (TextView) headerView.findViewById(R.id.nametextview);
        email = (TextView) headerView.findViewById(R.id.emailtextview);

        Fragment_Pagina fragment00 = new Fragment_Pagina();
        FragmentTransaction fragmentTransaction00 = getSupportFragmentManager().beginTransaction();
        fragmentTransaction00.replace(R.id.fram, fragment00, "Fragment Pagina");
        fragmentTransaction00.commit();

        miMenu = navigationView.getMenu();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                drawer.openDrawer(Gravity.LEFT, true);
            }
        }, 2500);

    }

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(firebaseAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(firebaseAuthListener != null){
            firebaseAuth.removeAuthStateListener(firebaseAuthListener);
        }
    }

    public void LogOut(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();

        firebaseAuth.signOut();
        goLogInScreen();
    }

    private void setUserData(final FirebaseUser user) {
        email.setText(user.getEmail());

        DocumentReference miDocRef = FirebaseFirestore.getInstance().document("Personal/" + user.getUid());

        miDocRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if(documentSnapshot.exists()){

                    name.setText(documentSnapshot.getString("nombre"));
                    foto.setImageBitmap(base64ToBitmap(documentSnapshot.getString("foto")));
                    cargo = documentSnapshot.getString("cargo");
                    TEMA = documentSnapshot.getString("tema");

                    validacionUsuario(name.getText().toString(), user.getUid(), TEMA);
                }
            }
        });

    }

    private Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

    private void goLogInScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        ventanaDialogo("Salir de la aplicación", "¿Seguro/a que desea salir?", "BackPressed");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_eventos) {
            FragmentEventos fragment0 = new FragmentEventos();
            FragmentTransaction fragmentTransaction0 = getSupportFragmentManager().beginTransaction();
            fragmentTransaction0.replace(R.id.fram, fragment0, "Fragment Eventos");
            fragmentTransaction0.commit();
        } else if (id == R.id.nav_comedor) {
            FragmentComedor fragment1 = new FragmentComedor();
            FragmentTransaction fragmentTransaction1 = getSupportFragmentManager().beginTransaction();
            fragmentTransaction1.replace(R.id.fram, fragment1, "Fragment Comedor");
            fragmentTransaction1.commit();
        } else if (id == R.id.nav_pagos) {
            IncertarPin();
        } else if (id == R.id.nav_descarga) {
            Fragment_Descargas fragment2 = new Fragment_Descargas();
            FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
            fragmentTransaction2.replace(R.id.fram, fragment2, "Fragment Descarga");
            fragmentTransaction2.commit();
        } else if (id == R.id.nav_notificacion) {
            Fragment_Notificacion fragment3 = new Fragment_Notificacion();
            FragmentTransaction fragmentTransaction3 = getSupportFragmentManager().beginTransaction();
            fragmentTransaction3.replace(R.id.fram, fragment3, "Fragment Notificacion");
            fragmentTransaction3.commit();
        } else if (id == R.id.nav_logout) {
            ventanaDialogo("Cerrar Sesión", "¿Seguro/a que desea cerrar sesión?", "LogOut");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void IncertarPin(){

        obtenerInfo();

        final Dialog d = new Dialog(this);
        d.setContentView(R.layout.pin_layout);
        final LinearLayout pinLayout;
        pinLayout = (LinearLayout) d.findViewById(R.id.LayoutPin);
        final PinEntryEditText pinEntry = (PinEntryEditText) d.findViewById(R.id.txt_pin_entry);
        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    if (str.toString().equals(pin)) {
                        Pagos();
                        d.cancel();
                    } else {
                        Snackbar.make(pinLayout, "Contraseña incorrecta",
                                Snackbar.LENGTH_LONG).setAction("", null).show();
                        pinEntry.setText(null);
                    }
                }
            });

            d.show();
        }

    }

    private void Pagos() {
        FragmentPagos fragment2 = new FragmentPagos();
        FragmentTransaction fragmentTransaction2 = getSupportFragmentManager().beginTransaction();
        fragmentTransaction2.replace(R.id.fram, fragment2, "Fragment Pagos");
        fragmentTransaction2.commit();
    }

    private void ventanaDialogo(String titulo, String mensaje, final String intencion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle(titulo);
        builder.setMessage(mensaje);

        builder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        Snackbar.make(coordinatorLayout, "Accion Desechada",
                                Snackbar.LENGTH_LONG).setAction("", null).show();

                    }
                });

        builder.setPositiveButton("Si",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        if(intencion.toString().equals("LogOut")){

                            LogOut();

                        }else if(intencion.toString().equals("BackPressed")){

                            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                            if (drawer.isDrawerOpen(GravityCompat.START)) {
                                drawer.closeDrawer(GravityCompat.START);
                            }
                            finish();
                        }

                    }
                });
        builder.show();

    }

    private void validacionUsuario(String name, String uid, String tema){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove("IDCorreo");
        editor.remove("Name");
        editor.remove("TEMA");
        editor.putString("IDCorreo", uid);
        editor.putString("Name", name);
        editor.putString("TEMA", tema);
        editor.apply();

        if(cargo.toString().equals("Profesor")){
            miMenu.findItem(R.id.nav_comedor).setEnabled(false);
            miMenu.findItem(R.id.nav_pagos).setEnabled(false);
        }else if(cargo.toString().equals("Director")){
            miMenu.findItem(R.id.nav_comedor).setEnabled(false);
            miMenu.findItem(R.id.nav_descarga).setEnabled(false);
            miMenu.findItem(R.id.nav_notificacion).setEnabled(false);
        }else if(cargo.toString().equals("Comedor")){
            miMenu.findItem(R.id.nav_descarga).setEnabled(false);
            miMenu.findItem(R.id.nav_pagos).setEnabled(false);
            miMenu.findItem(R.id.nav_eventos).setEnabled(false);
            miMenu.findItem(R.id.nav_notificacion).setEnabled(false);
        }
    }

    private void obtenerInfo() {

        DocumentReference miDocRef = FirebaseFirestore.getInstance().document("Pagos/importe");

        miDocRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if (documentSnapshot.exists()) {

                    pin = documentSnapshot.getString("pin");

                }
            }
        });

    }

}
