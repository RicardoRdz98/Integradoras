package com.orbita.innovacion.sva_escuela;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.HashMap;
import java.util.Map;

public class FragmentPagos extends Fragment {

    private String IDCorreo;
    private String nombre, tema;

    private Button publicar;
    private EditText monto, concepto;

    private LottieAnimationView success, error;

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pagos, container, false);

        publicar = (Button) v.findViewById(R.id.btnPublicar);
        monto = (EditText) v.findViewById(R.id.montoEditText);
        concepto =(EditText) v.findViewById(R.id.conceptoEditText);

        success = (LottieAnimationView) v.findViewById(R.id.success);
        error = (LottieAnimationView) v.findViewById(R.id.error);

        IDCorreo = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("IDCorreo","null");

        DocumentReference miDocRef = FirebaseFirestore.getInstance().document("Personal/" + IDCorreo);


        miDocRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if(documentSnapshot.exists()){

                    nombre = documentSnapshot.getString("nombre");
                    tema = documentSnapshot.getString("tema");

                }else{
                }
            }
        });

        publicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(monto.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo monto vacío", Toast.LENGTH_SHORT).show();
                    return;
                }else if(concepto.getText().toString().equals("")){
                    Toast.makeText(getContext(), "Campo concepto vacío", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    subir();
                    publicar.setEnabled(false);
                }
            }
        });

        return v;
    }

    private void subir() {
        Double cash = Double.valueOf(monto.getText().toString());
        String caso = concepto.getText().toString();

        Map<String, Object> pagos = new HashMap<>();
        pagos.put("dinero", cash);
        pagos.put("intencion", caso);

        db.collection("Pagos").document("importe")
                .update(pagos)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        success.setVisibility(View.VISIBLE);
                        success.playAnimation();
                        monto.setText(null);
                        concepto.setText(null);
                        publicar.setEnabled(true);
                        NotificarPhpDirectivos NPD =
                                new NotificarPhpDirectivos(
                                        getContext(),
                                        tema,
                                        nombre,
                                        " a activiado el apartado de pagos",
                                        "Entre a su aplicación y verifique el servicio");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        error.setVisibility(View.VISIBLE);
                        error.playAnimation();
                    }
                });
    }
}
