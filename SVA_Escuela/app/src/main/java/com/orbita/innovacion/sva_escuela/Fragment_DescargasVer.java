package com.orbita.innovacion.sva_escuela;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class Fragment_DescargasVer extends Fragment {

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    private ProgressBar progressBar;
    private RecyclerView.Adapter adapter;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager lManager;

    private ArrayList<Item_Descarga> items = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /*Método que realizará las descargas de los horarios de los alumnos
    * registrados en una aula de clases*/
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_descargas_ver, container, false);

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);

        items = new ArrayList<Item_Descarga>();

        // Obtener el RecyclerView
        recycler = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        recycler.setHasFixedSize(true);

        // Llamada a la coleccion de Horarios para traer todos los datos
        db.collection("Horarios").get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (DocumentSnapshot document : task.getResult()) {
                                items.add(new Item_Descarga(document.getString("grupo"),
                                        document.getString("contenido"),
                                        document.getString("img"),
                                        document.getId()));
                            }

                            // Usar un administrador para LinearLayout
                            lManager = new LinearLayoutManager(getContext());
                            recycler.setLayoutManager(lManager);

                            // Crear un nuevo adaptador
                            adapter = new AdapterListItem(items);
                            recycler.setAdapter(adapter);

                            progressBar.setVisibility(View.GONE);
                            recycler.setVisibility(View.VISIBLE);
                        }
                    }
                });

        return v;
    }

    //Clase anidada para adaptar datos al CardView y RecyclerView
    public class AdapterListItem extends RecyclerView.Adapter<AdapterListItem.HorarioViewHolder> {
        //Creacion de lista de items
        private List<Item_Descarga> items;

        public class HorarioViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            // Campos respectivos de un item
            private ImageView imagen;
            private TextView titulo;
            private TextView contenido;
            private Button descargar;
            private TextView id;
            private Context context;

            public HorarioViewHolder(View v) {
                //Inicilizando WidGets del CardView (item_descargas)
                super(v);
                context = v.getContext();
                id = (TextView) v.findViewById(R.id.txt_id_descargas);
                imagen = (ImageView) v.findViewById(R.id.imgCard);
                titulo = (TextView) v.findViewById(R.id.txtTituloCard);
                contenido = (TextView) v.findViewById(R.id.txtConteCard);
                descargar = (Button) v.findViewById(R.id.btnEliminar);
            }
            public void accesoAbotones(){
                descargar.setOnClickListener(this);
            }

            // Boton de descarga de imagen de horario para su mejor acceso
            @Override
            public void onClick(View v) {
                if(v.getId() == R.id.btnEliminar){
                    ventanaDialogo(id.getText().toString());
                }
            }

            public void ventanaDialogo(final String ID) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                builder.setTitle("Eliminacion");
                builder.setMessage("¿Seguro/a que desea eliminar este horario?");

                builder.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Toast.makeText(context, "Acción Desechada", Toast.LENGTH_SHORT).show();

                            }
                        });

                builder.setPositiveButton("Si",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                db.collection("Horarios").document(ID)
                                        .delete()
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Toast.makeText(context, "Eliminado con exito", Toast.LENGTH_SHORT).show();

                                                Fragment nuevoFragmento = new Fragment_DescargasVer();
                                                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                                transaction.replace(R.id.fram, nuevoFragmento);
                                                transaction.addToBackStack(null);
                                                transaction.commit();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(context, "Error al eliminar", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                            }
                        });
                builder.show();
            }
        }

        public AdapterListItem(List<Item_Descarga> items) {
            //Igualando datos de la lista
            this.items = items;
        }

        @Override
        public int getItemCount() {
            //Sacando tamaño de la lista
            return items.size();
        }

        @Override
        public HorarioViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            //Inflando Layout de CardView (item_descargas)
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_descargas, viewGroup, false);
            return new HorarioViewHolder(v);
        }

        @Override
        public void onBindViewHolder(HorarioViewHolder viewHolder, int i) {
            //Agregando los datos al CardView (item_descargas)
            //y dando paso a los eventos onClickListener del Boton descargar
            viewHolder.imagen.setImageBitmap(items.get(i).getImg());
            viewHolder.id.setText(items.get(i).getID());
            viewHolder.titulo.setText(items.get(i).getTitulo());
            viewHolder.contenido.setText(items.get(i).getContenido());
            viewHolder.accesoAbotones();
        }
    }

}
